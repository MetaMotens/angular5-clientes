import {Cliente} from './cliente';

export const CLIENTES: Cliente[] = [
  {id: 1, nombre: 'Andres', apellido: 'Guzman', email: 'profesor@bolsadeideas.com', createAt: '2017-08-01'},
  {id: 1, nombre: 'John', apellido: 'Doe', email: 'john.doe@gmail.com', createAt: '2017-08-02'},
  {id: 1, nombre: 'Linus', apellido: 'Torvalds', email: 'linus.torvalds@gmail.com', createAt: '2017-08-03'},
  {id: 1, nombre: 'Jane', apellido: 'Doe', email: 'jane.doe@gmail.com', createAt: '2017-08-04'},
  {id: 1, nombre: 'Rasmus', apellido: 'Lerdorf', email: 'rasmus.lerdorf@gmail.com', createAt: '2017-08-05'},
  {id: 1, nombre: 'Erich', apellido: 'Gamma', email: 'erich.gamma@gmail.com', createAt: '2017-08-06'},
  {id: 1, nombre: 'Richard', apellido: 'Helm', email: 'richard.helm@gmail.com', createAt: '2017-08-07'},
  {id: 1, nombre: 'Ralph', apellido: 'Johnson', email: 'ralph.johnson@gmail.com', createAt: '2017-08-08'},
  {id: 1, nombre: 'John', apellido: 'Vlissides', email: 'john.vlissides@gmail.com', createAt: '2017-08-09'},
  {id: 1, nombre: 'James', apellido: 'Gosling', email: 'james.gosling@gmail.com', createAt: '2017-08-10'},
]
