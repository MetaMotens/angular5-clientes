import { Injectable } from '@angular/core';
import { CLIENTES } from './clientes.json';
import { Cliente } from './cliente';
import { Observable, of} from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';



@Injectable(/*{
  providedIn: 'root'
}*/)
export class ClienteService {

  private urlEndPoint = 'http://localhost:8080/api/clientes';

  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'}) // en teoria en angular 7 no hace falta el content??

  constructor(private http: HttpClient) { }

  getClientes(): Observable<Cliente[]> {

    // return of(CLIENTES);
    // return this.http.get<Cliente[]>(this.urlEndPoint); Forma 1 de obtener y convertir el get al tipo cliente.
    return this.http.get(this.urlEndPoint).pipe
    (
      map( (response) => response as Cliente[] ) // Forma 2 de obtener y convertir el get al tipo cliente.
    );
  }

  create(cliente: Cliente): Observable<Cliente> {
    return this.http.post<Cliente>(this.urlEndPoint, cliente, {headers: this.httpHeaders});
  }

  getCliente(id): Observable<Cliente> {
    return this.http.get<Cliente>(`${this.urlEndPoint}/${id}`);
  }

  update(cliente: Cliente): Observable<Cliente> {
    return this.http.put<Cliente>(`${this.urlEndPoint}/${cliente.id}`, cliente, {headers: this.httpHeaders});
  }

  delete(id: number): Observable<Cliente> {
    return this.http.delete<Cliente>(`${this.urlEndPoint}/${id}`, {headers: this.httpHeaders});
  }
}
